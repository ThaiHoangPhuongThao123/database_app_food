-- DATABASE app_food
CREATE DATABASE db_app_food_file;
USE db_app_food_file;

-- DROP TABLE
DROP TABLE orders;
DROP TABLE sub_food;
DROP TABLE food;
DROP TABLE food_type;
DROP TABLE rate_res;
DROP TABLE like_res;
DROP TABLE restaurants;
DROP TABLE users;


-- USER
CREATE TABLE users (
user_id INT PRIMARY KEY AUTO_INCREMENT,
full_name VARCHAR(300),
email VARCHAR(400),
password VARCHAR(100)
);
-- RETAURANTS
CREATE TABLE restaurants (
res_id INT PRIMARY KEY AUTO_INCREMENT,
res_name VARCHAR(500),
image VARCHAR(1000),
description VARCHAR(1000)
);

-- LIKE-RES
CREATE TABLE like_res (
user_id INT,
res_id INT,
date_like DATETIME,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (res_id) REFERENCES restaurants(res_id)
);
-- RATE RES
CREATE TABLE rate_res (
user_id INT,
res_id INT,
amount INT,
date_rate DATETIME,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (res_id) REFERENCES restaurants(res_id)
);

-- FOOD_TYPE
CREATE TABLE food_type (
type_id INT PRIMARY KEY AUTO_INCREMENT,
type_name VARCHAR(500)
);

-- FOOD
CREATE TABLE food (
food_id INT PRIMARY KEY AUTO_INCREMENT,
food_name VARCHAR(500),
image VARCHAR(1000),
price FLOAT,
description VARCHAR(1000),
type_id INT,
FOREIGN KEY (type_id) REFERENCES food_type(type_id)
);

-- SUB_FOOD
CREATE TABLE sub_food (
sub_id INT PRIMARY KEY AUTO_INCREMENT,
sub_name VARCHAR(500),
sub_price FLOAT,
food_id INT,
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

-- ORDERS
CREATE TABLE orders (
user_id INT,
food_id INT,
amount INT,
code VARCHAR(1000),
arr_sub_id VARCHAR(1000),
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

INSERT INTO users(full_name,email, password ) VALUES 
('Nguyễn Văn A', 'a@ggmail.com', '1234a'),
('Nguyễn Văn Bảnh', 'banh@ggmail.com', '1234banh'),
('Nguyễn Văn Huệ', 'hue@ggmail.com', '1234hue'),
('Lê Ngọc Hoa', 'ngochoa@ggmail.com', '1234hoa'),
('Nguyễn Ngọc Mai', 'mai@ggmail.com', '1234mai'),
('Trần Tố Uyên', 'uyen@ggmail.com', '123uyen'),
('Phạm Hà Thanh', 'thanh@ggmail.com', 'thanhthanh'),
('Phạm Văn Hùng', 'hung@ggmail.com', 'hung44');

INSERT INTO restaurants(res_name,image, description ) VALUES 
('Hệ thống Nhà hàng Phố 79', 'https://cdn.justfly.vn/1920x1281/media/202104/26/1619433770-he-thong-nha-hang-pho-79-sai-gon.jpg', 'Kết hợp giữa kiến trúc hiện đại của phương Tây và nét hoài cổ Việt'),
('Nhà hàng Maison Mận-Đỏ', 'https://cdn.justfly.vn/1920x1283/media/202302/27/1677500379-khong-gian-nha-hang-maison-mando.jpg', 'Với lối kiến trúc Đông Dương độc đáo, sang trọng giao thoa giữa lối kiến trúc Châu Âu'),
('Chuỗi Ân Nam Quán', 'https://cdn.justfly.vn/1920x1081/media/07/19/3a0e-392d-478a-b583-67c382a011a5.jpg', 'Thực đơn của chuỗi Ân Nam Quán có tới hơn 80 món ăn đặc sản nổi tiếng của miền Trung'),
('Nhà hàng Sườn Nướng Hàn Quốc', 'https://cdn.justfly.vn/1920x1280/media/30/f7/d082-5653-4666-8b2f-70ad2e7b229c.jpg', 'Sườn Nướng Hàn Quốc trở thành quán ăn ngon thu hút khá đông giới trẻ Sài Gòn'),
(' Chuỗi Lẩu Cua Đất Mũi', 'https://cdn.justfly.vn/1920x1280/media/39/68/a5b7-83f3-402a-9062-a1853d5d13af.jpg', ' Hầu hết các thực khách đến với nhà hàng này là nhờ thực đơn vô cùng hấp dẫn'),
('Chuỗi Wrap & Roll', 'https://cdn.justfly.vn/1920x1440/media/e5/23/29e8-d3a1-4d35-aaa8-bc3ec5864260.jpg', 'Wrap & Roll đều được tọa lạc ở những vị trí sang trọng và lịch sự'),
('Chuỗi San Fu Lou', 'https://cdn.justfly.vn/1920x1245/media/b6/15/d2f4-38b6-403b-b477-fc6b1cb4c5f4.jpg', 'San Fu Lou là chuỗi nhà hàng mang phong cách cổ điển Trung Hoa cao cấp'),
('Chuỗi Dìn Ký', 'https://cdn.justfly.vn/1920x1281/media/fa/0b/301f-f251-4524-90fd-61d1b46cc29b.jpg','Gần 400 món ăn đặc sắc với đa dạng phong cách ẩm thực');

INSERT INTO like_res(user_id,res_id,date_like ) VALUES 
(5,1,'2020-01-01 10:10:10'),
(4,1,'2020-02-13 23:40:20'),
(2,5,'2020-05-09 10:42:11'),
(4,1,'2020-11-25 09:40:10'),
(1,1,'2020-09-05 15:30:10'),
(7,2,'2020-06-06 18:16:16'),
(5,6,'2020-07-07 10:10:17'),
(2,2,'2020-09-01 19:10:19'),
(6,4,'2020-01-01 10:10:10'),
(3,1,'2020-10-09 11:16:10'),
(6,1,'2020-07-08 09:10:06'),
(3,3,'2020-02-02 12:12:12');


INSERT INTO rate_res (user_id,res_id,amount, date_rate ) VALUES 
(5,1,4,'2020-01-01 10:11:10'),
(4,1,4,'2020-02-13 23:41:20'),
(2,5,3,'2020-05-09 10:45:11'),
(4,1,5,'2020-11-25 09:42:10'),
(1,1,3,'2020-09-05 15:33:10'),
(7,2,4,'2020-06-06 18:26:16'),
(5,6,5,'2020-07-07 10:18:17'),
(2,2,4,'2020-09-01 19:15:19'),
(6,4,3,'2020-01-01 10:15:10'),
(3,1,4,'2020-10-09 11:13:10'),
(6,1,5,'2020-07-08 09:19:06'),
(3,3,4,'2020-02-02 12:14:12');

INSERT INTO food_type(type_name) VALUES 
('Món chính'),
('Món Phụ'),
('Nước Uống'),
('Tráng miệng');

INSERT INTO food(food_name, image,price, description, type_id ) VALUES 
('Thịt heo chiên xóc tỏi','https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2018/August/31/1317-895901535701440-1535701440.jpg',2000, 'Thịt heo chiên xóc tỏi còn biến tấu để trở thành món mồi nhắm cực kỳ tuyệt', 1),
('Thịt heo hầm cà rốt','https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2018/August/31/1333-929801535701908-1535701908.jpg',2500, 'Vị ngọt thanh của cà rốt kết hợp với những miếng thịt lợn đậm đà sẽ là món ăn vô cùng hấp dẫn', 1),
('Thịt bò xào hành tây','https://cdn.tgdd.vn/2021/04/content/thitboxaohanhtay-800x450.jpg',3000, 'Thịt bò và hành tây là bộ đôi kết hợp cực ăn ý trong rất nhiều món ăn ngon', 1),
('Thịt bò xào lúc lắc','https://cdn.tgdd.vn/2021/04/content/thitboxaoluclac-800x450.jpg',2000, 'Mùi thơm của ớt chuông càng khiến thịt bò thêm dậy mùi', 1),
('Mực ống chiên nước mắm','https://cdn.tgdd.vn/2021/05/content/1-800x450-68.jpg',3200, 'Mực được chiên nhưng vẫn giữ được độ dai giòn, tươi ngon', 1),
('Mực chiên muối tắc','https://cdn.tgdd.vn/2021/05/content/6-800x450-21.jpg',2500, 'Mực chiên muối tắc có lớp vỏ vàng giòn', 1),
('Gà nướng muối ớt','https://cdn.tgdd.vn/Files/2020/11/30/1310299/tong-hop-cac-mon-ngon-tu-ga-sieu-ngon-de-nau-tai-nha-202203161150216336.jpg',2500, ' thịt gà mềm thấm vị mặn của muối, cay nồng của ớt', 1),
('Tôm hùm nướng bơ tỏi','https://naifood.com/wp-content/uploads/2022/11/cac-mon-tu-tom-hum-hap-2.jpg',5000, 'Kết hợp với tỏi nướng giòn sần sật', 1),
('Tôm hùm nướng phô mai','https://naifood.com/wp-content/uploads/2022/11/cac-mon-tu-tom-hum-nuong-2.jpg',5300, 'tôm nướng phô mai ghi dấu ấn với vị béo ngậy và thơm lừng của phô mai', 1),
('Cá hồi sốt cam','https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2020/December/11/7235-955221607652203-1607652203.jpg',4000, 'Thịt cá hồi mềm thơm quyện cũng vị chua chua, ngọt ngọt, thơm thơm của sốt cam', 1),
('Coca-Cola','https://songseafoodgrill.vn/wp-content/uploads/2022/03/Coca-2-600x560.png',500, 'Nước ngọt Coca-Cola', 3),
('Chè trái cây','https://imgs.vietnamnet.vn/Images/2017/10/12/15/20171012152957-che-trai-cay-chua-ngot.jpg',600, 'Chè trái cây phục vụ sau bữa tối', 4),
('Bánh khoai tây','https://i1-giadinh.vnecdn.net/2021/08/27/nh3-1630054035-4780-1630054195.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=mw8F2sVYgu1wjwc33-aQGw',600, 'Khoai tây chiên giòn', 2),
('Canh hấp','https://vifon.com.vn/vnt_upload/kitchen/09_2019/thumbs/570_crop_canhrieuthanhmat-Copy.jpg',800, 'Canh hấp thơm ngon', 2);

INSERT INTO sub_food (sub_name, sub_price, food_id) VALUES 
('Bánh khoai tây',600, 13),
('Canh cà chua',800, 14),
('Coca-Cola',500, 11);

INSERT INTO orders (user_id, food_id, amount, code, arr_sub_id) VALUES 
(1,14, 2,'ORD123','1-2'),
(1,5, 2,'ORD124','3'),
(3,5, 2,'ORD125','1-2'),
(4,4, 2,'ORD126','1-3'),
(7,14, 2,'ORD127','3'),
(5,6, 2,'ORD128','2-3');


-- TÌM 5 NGƯỜI ĐÃ LIKE NHÀ HÀNG NHIỀU NHẤT
SELECT users.user_id, users.full_name, COUNT(users.user_id) AS number_of_like
FROM users
INNER JOIN like_res
ON like_res.user_id = users.user_id
INNER JOIN restaurants
ON restaurants.res_id = like_res.res_id
GROUP BY users.user_id
ORDER BY number_of_like DESC
LIMIT 5;

-- TÌM 2 NHÀ HÀNG CÓ LƯỢT LIKE NHIỀU NHẤT
SELECT restaurants.res_id, restaurants.res_name, COUNT(restaurants.res_id) AS total_likes
FROM restaurants
INNER JOIN like_res
ON restaurants.res_id = like_res.res_id
INNER JOIN users
ON users.user_id = like_res.user_id
GROUP BY restaurants.res_id
ORDER BY total_likes DESC
LIMIT 2;

-- TÌM NGƯỜI ĐẶT HÀNG NHIỀU NHẤT

SELECT users.user_id, users.full_name, COUNT(users.user_id) AS total_orders
FROM users
INNER JOIN orders 
ON users.user_id = orders.user_id
GROUP BY users.user_id
ORDER BY total_orders DESC
LIMIT 1;

-- TÌM NGƯỜI  DÙNG KHÔNG ĐẶT HÀNG
SELECT users.user_id, users.full_name
FROM users
LEFT JOIN orders
ON users.user_id = orders.user_id
WHERE orders.user_id IS NULL;

-- TÌM NGƯỜI DÙNG KHÔNG LIKE 
SELECT users.user_id, users.full_name
FROM users
LEFT JOIN like_res
ON users.user_id = like_res.user_id
WHERE like_res.user_id IS NULL;

-- TÌM NGƯỜI DÙNG KHÔNG ĐÁNH GIÁ NHÀ HÀNG 
SELECT users.user_id, users.full_name
FROM users
LEFT JOIN rate_res
ON users.user_id = rate_res.user_id
WHERE rate_res.user_id IS NULL;






